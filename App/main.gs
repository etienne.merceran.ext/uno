var SHEET = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
var RANK_PART = SHEET.getRange("B2:B").getValues();
var FILTERED_INPUT = RANK_PART.filter(String);
var TOTAL_PLAYERS = parseInt(FILTERED_INPUT.length);
   
function onOpen(e) {
  SpreadsheetApp.getUi()
  .createMenu("Nouvelle partie")
  .addItem("Play", "newPart")
  .addSeparator()
  .addItem("Les Règles", "showReadmeSideBar")
  .addToUi();
}

function showReadmeSideBar(){
  // onOpen trigger who help to display a sidebar with custom HtmlService content
  var htmlOutput = HtmlService.createHtmlOutputFromFile("readme_before.html")
  .setTitle("Uno stats DF");
  SpreadsheetApp.getUi().showSidebar(htmlOutput);
}

function onEdit(e) {
   var cell = e.range.getA1Notation();
   var cellAdress = cell.substring(0, 1);
 
   if(cellAdress=="B"){ 
    if (parseInt(e.value)>=1 && parseInt(e.value) <= TOTAL_PLAYERS) {
      Logger.log("value: "+e.value)
    }else {
      var ui = SpreadsheetApp.getUi();
      var result = ui.alert(
        "Advise 🚨",
        "Entrez svp les rangs dans l'ordre croissant 1...n ",
        ui.ButtonSet.OK);
        SHEET.getRange(cell).setValue("")
    }
  }
}

function newPart() {
  Logger.log("rank_part: ", RANK_PART);
  Logger.log("filtered_input: ", FILTERED_INPUT);
  
  var ui = SpreadsheetApp.getUi();
  if (TOTAL_PLAYERS < 4) {
    // We need at least 4 players to start a new part
    var result = ui.alert(
        "Advise 🚨",
        "Vous avez besoin d'au moins 4 joueurs pour démarrer une partie",
        ui.ButtonSet.OK);
        
    SHEET.getRange("B2:B").setValue("")
  } 
  else {
    r = Math.ceil(TOTAL_PLAYERS/2)
    for (var i = 0; i < RANK_PART.length; i += 1) {
      if (RANK_PART[i][0] != "") {
        SHEET.getRange("C"+(i+2)).setValue(SHEET.getRange("C"+(i+2)).getValue() + (r + (1 - (RANK_PART[i][0]))))
        SHEET.getRange("D"+(i+2)).setValue(SHEET.getRange("D"+(i+2)).getValue() + 1)
        SHEET.getRange("B2:B").setValue("")
      }
    }
  }
}
