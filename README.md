## Règles du Uno version DF
Les règles de base s'appliquent, excepté ce qui suit.

### Interception ou pyjama
Peu importe à qui c’est le tour, si on a **exactement** la même carte (symbole et couleur), on peut la jouer. Le joueur suivant est celui après l’intercepteur dans l’ordre du tour.
**Non applicable pour les +4 et les changements de couleur.**

### Cumul des cartes
A notre tour de jeu, ou après une interception, on peut jouer plusieurs cartes en même temps si elles ont le même symbole (peu importe la couleur).
**Non applicable pour les +4 et les changements de couleur.**

### Jouer un +2
Le joueur qui pioche **n’a pas le droit de jouer**.

### Contrer un +2
On peut jouer un +2 si quelqu’un nous a mis un +2. Le joueur suivant pioche donc la somme de tous les +2. Il est bien sûr possible de cumuler les +2.

### Jouer un +4
On ne peut jouer un +4 **que si on n’a pas la couleur ni le symbole**. Le joueur qui doit piocher les 4 cartes peut dire s’il y a bluff : le joueur qui a joué le +4 montre son jeu, et **s’il a bluffé alors il pioche 6 cartes**, sinon le joueur à qui s’appliquait le +4 en pioche 6 à la place. Le joueur à qui s’appliquait le +4 ne peut pas jouer.

### Contrer un +4
Impossible de jouer un +4 pour en contrer un autre.

### Piocher 1 carte
Si on a pas la carte, ou pour le plaisir, on peut piocher une carte. On a le droit de jouer tout de suite la carte piochée si possible.

### Triche 
Si on se fait prendre en train de tricher, **on doit piocher 2 cartes** (uno abusif, contre uno abusif, mauvaise carte jouée, pas à son tour de jouer...)

### Carte de fin
On peut finir le jeu sur n’importe quelle carte.

### Contre-uno
Les joueurs qui ont fini de jouer ont aussi le droit de dire contre-uno. Si le contre-uno est abusif, **le joueur revient donc en jeu** avec 2 cartes.

### Fin de jeu pour un joueur
Il est interdit de jouer en tant que dernière carte un +4 ou un changement de couleur.
Si un joueur a posé un +2 en dernière carte et que celui-ci a été contré et que le cumul des +2 revient vers le joueur qui a posé sa dernière carte, alors le joueur n'est pas sorti et doit piocher le cumul des +2
